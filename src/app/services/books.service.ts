import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { Book } from '../shered/books.interface';

@Injectable({
    providedIn: 'root'
  })
  
export class BooksService {
    constructor(private db: AngularFirestore) { }

    private book: Book;
   
    booksArray: any = [
        {
            title:'Alice in Wonderland',
            author:'Lewis Carrol'
        },
        {
            title:'War and Peace',
            author:'Leo Tolstoy'
        }, 
        {
            title:'The Magic Mountain',
            author:'Thomas Mann'
        },
        {
            title:'The Magic Mountain 2',
            author:'Thomas Mann'
        }
];
getBooks() {
    return this.booksArray;
  }

getBooksFromDB():Observable<any[]>{
    return this.db.collection('books').valueChanges({ idField: 'id' });
}
addBooks(title: string, author: string){
    this.book = {title: title, author: author};
    this.db.collection('books').add(this.book);
    }

editBooks(title: string, author: string, id: any){
    return this.db.collection('books').doc(id).update({
        title: title,
        author: author
        });
    }
deleteBook(id){
    if(id !== undefined){
        this.db.collection("books").doc(id).delete();
        }
    }
}