import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Weather,WeatherJson} from '../shered/weatherInterface'

import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class TemperaturesService {

    private URL = "https://api.openweathermap.org/data/2.5/weather?q=";
    private KEY = "c1dd35fe731dba13909b2635298f083e";
    private IMP = "&units=metric";
    
    constructor(private http: HttpClient){}

    searchData(city: string): Observable<Weather>{
        return this.http.get<WeatherJson>(`${this.URL}${city}&APPID=${this.KEY}${this.IMP}`)
        .pipe(
            map(data => this.transformData(data))
        );
    }

    private transformData(data:WeatherJson):Weather{
        return {
          name:data.name,
          country:data.sys.country,
          image: `http://api.openweathermap.org/img/w/${data.weather[0].icon}.png`,
          description:data.weather[0].description,
          temperature: data.main.temp,
          lat: data.coord.lat,
          lon: data.coord.lon
        }
      }
}