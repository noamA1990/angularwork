import { EditBookComponent } from './edit-book/edit-book.component';
import { UpdateTemperatureComponent } from './update-temperature/update-temperature.component';
import { TemperaturesComponent } from './temperatures/temperatures.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BooksComponent } from './books/books.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { AddBookComponent } from './add-book/add-book.component';
import { LogInComponent } from './log-in/log-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { VerifyEmailComponent } from './verify-email/verify-email.component';
import { AuthGuard } from './shered/guard/auth.guard';
import { SecureInnerPagesGuard } from './shered/guard/secure-inner-pages.guard';

const appRoutes: Routes =[
    { path: '', redirectTo: '/log-in', pathMatch: 'full' },
    { path: 'log-in', component: LogInComponent },
    { path: 'register-user', component: SignUpComponent, canActivate: [SecureInnerPagesGuard]},
    // { path: 'dashboard', component: DashboardComponent },
    { path: 'forgot-password', component: ForgotPasswordComponent, canActivate: [SecureInnerPagesGuard] },
    { path: 'verify-email-address', component: VerifyEmailComponent, canActivate: [SecureInnerPagesGuard] },

    // { path: '', component: BooksComponent },
    { path: 'books', component: BooksComponent, canActivate: [AuthGuard] },
    { path: 'addBook', component: AddBookComponent },
    { path: 'books/:id/:title/:author/edit', component: EditBookComponent },
    // { path:'temperatures/:val/:city', component: TemperaturesComponent },
    { path:'temperatures/:city', component: TemperaturesComponent },
    { path: 'update-temperature', component: UpdateTemperatureComponent },
   
    { path: 'not-found', component:ErrorPageComponent, data: {message: 'Page not found!'} },
    { path: '**', redirectTo: '/not-found' }
  ]
@NgModule({
    imports: [
        // RouterModule.forRoot(appRoutes, {useHash :true})
        RouterModule.forRoot(appRoutes)

    ],
    exports: [RouterModule]
})

export class AppRoutingModule{}