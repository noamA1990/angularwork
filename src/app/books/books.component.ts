import { Component, OnInit, Output, Inject } from '@angular/core';

import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { BooksService } from '../services/books.service';

export interface DialogData {
  title: string;
  key: any;
}

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  @Output() panelOpenState = false;

  books: any;
  books$;
  key;
  title:string;

  constructor(private bookService: BooksService, private dialog: MatDialog) { }
  
  delete(title: string, key:any){
    this.title = title;
    this.key = key;
    this.openDialog();
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '250px',
      height: '200px',
      data: {title:this.title, key:this.key}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.bookService.deleteBook(result);
    });
  }

  ngOnInit() {
    // this.books = this.bookService.getBooks();
    this.books$ = this.bookService.getBooksFromDB();
  }
}

@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'dialog-overview-example-dialog.html',
})
export class DialogOverviewExampleDialog {
 
  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}