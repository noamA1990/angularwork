import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Weather } from '../shered/weatherInterface';
import { TemperaturesService } from '../services/temperatures.service';

@Component({
  selector: 'app-temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})
export class TemperaturesComponent implements OnInit {
  likes = 0;
  // value: number;
  city;
  temp;
  image;
  dataMain$: Observable<Weather>;

  addLikes(){
    this.likes++;
  }

  constructor(private route:ActivatedRoute, private tempService:TemperaturesService) { }

  ngOnInit() {
    // this.value = this.route.snapshot.params['val'];
    this.city = this.route.snapshot.params['city'];
    this.dataMain$ = this.tempService.searchData(this.city);
    this.dataMain$.subscribe(
      data => {
        this.image = data.image;
        this.temp = data.temperature;
      }
    )
  }
}
