import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

import { Router, ActivatedRoute } from '@angular/router';
import { BooksService } from '../services/books.service';

@Component({
  selector: 'app-edit-book',
  templateUrl: './edit-book.component.html',
  styleUrls: ['./edit-book.component.css']
})
export class EditBookComponent implements OnInit {
  
  title: string;
  author: string;
  id;
  editTitle = new FormControl('', [Validators.required]);
  editAuthor = new FormControl('', [Validators.required]);
  getBookErrorMessage() {
    return this.editTitle.hasError('required') ? 'You must enter a book title' : '';
  }
  getAuthorErrorMessage() {
    return this.editAuthor.hasError('required') ? 'You must enter a author name' : '';
  }
  constructor(private bookService: BooksService, private router: Router, private route:ActivatedRoute) { }

  ngOnInit() {
    this.title = this.route.snapshot.params['title'];
    this.author = this.route.snapshot.params['author'];
    this.id = this.route.snapshot.params['id'];
  }

  editBook(){
    this.bookService.editBooks(this.title, this.author, this.id);
    this.router.navigate(['/books']);
  }

}
