export interface Weather{
    name:String,
    country:String,
    image:String,
    description:String,
    temperature:number,
    lat?:number,
    lon?:number  
}
export interface WeatherJson{
    weather: [
        {
          description: string
          icon: string
        }
      ];
      main: {
        temp: number
      };
      sys: {
        country: string
      };
      coord: {
        lon: number,
        lat: number
      };
      name: string;    
}