import { AuthService } from './services/auth.service';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MatSliderModule } from '@angular/material/slider';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SideMenuComponent } from './side-menu/side-menu.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { BooksComponent, DialogOverviewExampleDialog } from './books/books.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { TemperaturesComponent } from './temperatures/temperatures.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { AppRoutingModule } from './app-routing.module';
import {MatCardModule} from '@angular/material/card';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule, MatDialog, MatDialogModule} from '@angular/material';
import {MatFormFieldModule} from '@angular/material/form-field';
import { UpdateTemperatureComponent } from './update-temperature/update-temperature.component';
import { FormsModule } from '@angular/forms';
import { AddBookComponent } from './add-book/add-book.component';

import { HttpClientModule } from '@angular/common/http';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from "@angular/fire/auth";
import { environment } from 'src/environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { EditBookComponent } from './edit-book/edit-book.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { LogInComponent } from './log-in/log-in.component';
import { TemperaturesService } from './services/temperatures.service';
import { BooksService } from './services/books.service';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { VerifyEmailComponent } from './verify-email/verify-email.component';
import { FlexLayoutModule } from '@angular/flex-layout';


@NgModule({
  declarations: [
    AppComponent,
    SideMenuComponent,
    BooksComponent,
    TemperaturesComponent,
    ErrorPageComponent,
    UpdateTemperatureComponent,
    AddBookComponent,
    EditBookComponent,
    DialogOverviewExampleDialog,
    SignUpComponent,
    LogInComponent,
    ForgotPasswordComponent,
    VerifyEmailComponent,

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatSliderModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatExpansionModule,
    MatCardModule,
    AppRoutingModule,
    MatFormFieldModule,
    MatSelectModule,
    FormsModule,
    MatInputModule,
    MatDialogModule,
    HttpClientModule,
    AngularFireModule,
    AngularFirestoreModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    FlexLayoutModule,
    ],
  providers: [BooksService, TemperaturesService,MatDialog, AuthService],
  bootstrap: [AppComponent],
  entryComponents: [DialogOverviewExampleDialog ]
})
export class AppModule { }
