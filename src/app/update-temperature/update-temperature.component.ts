import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { Router } from '@angular/router';

export interface City{
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-update-temperature',
  templateUrl: './update-temperature.component.html',
  styleUrls: ['./update-temperature.component.css']
})

export class UpdateTemperatureComponent implements OnInit{
  // temperature: number;
  city:string;
  
  temp = new FormControl('', [Validators.required]);
  
  cities: City[] = [
    {value: 'Jerusalem', viewValue: 'Jerusalem'},
    {value: 'London', viewValue: 'London'},
    {value: 'Paris', viewValue: 'Paris'}
  ];

  getErrorMessage() {
    return this.temp.hasError('required') ? 'You must enter a temperature' : '';
  }
  constructor(private router: Router){}

  ngOnInit() {
  }

  onSubmit(){
    this.router.navigate(['/temperatures',this.city]);
  }
}
