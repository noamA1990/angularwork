import { AuthService } from './../services/auth.service';
import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.css']
})
export class SideMenuComponent {
title: string;
isAuthenticated = false;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver, location: Location, router: Router,
    public authService: AuthService) {
    router.events.subscribe(val => {
      if (location.path() == "/books" || location.path() == "") {
        this.title = 'Books';
      } 
      else if(location.path() == "/addBook"){
        this.title = 'Add new book';
      }
      else if (location.path().endsWith('edit')) {
        this.title = "Edit book";
      }
      else if(location.path() == "/update-temperature"){
        this.title = "Update temperature";
      }
      else if(location.path() == "/log-in"){
        this.title = "Log In Page";
      }
      else if(location.path() == "/register-user"){
        this.title = "Sign up page";
      }
      else if(location.path() == "/forgot-password"){
        this.title = "Reset Password";
      }
      else {
        this.title = "Temperatures";
      }
    });   
  }
  SignOut(){
    this.authService.SignOut();
  }
}
