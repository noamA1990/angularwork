// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDDduakjsEXiYn2iOrGOaI3I02mfCNLEb0",
    authDomain: "classworks-f3eb3.firebaseapp.com",
    databaseURL: "https://classworks-f3eb3.firebaseio.com",
    projectId: "classworks-f3eb3",
    storageBucket: "classworks-f3eb3.appspot.com",
    messagingSenderId: "418505698028",
    appId: "1:418505698028:web:89f8671e81d7d07fcdd3c9",
    measurementId: "G-21YC7DF01L"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
